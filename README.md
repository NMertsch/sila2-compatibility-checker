# SiLA 2 Interoperability Testing
## Testing principles
### Separation of connection and communication tests
Tests are split in two main categories:
1. **Connection tests** - A test is successful if a SiLA Client can connect to a SiLA Server and communicate with it
2. **Communication tests** - A test starts with an established connection and tests the content of communication (request message, request metadata, response message, response status)

### Source of truth
This repository will provide testing tools which can be used to check if a given SiLA 2 implementation satisfies various parts of the SiLA 2 specification.
The SiLA 2 specification is the source of truth for all these tests.

We do our best to keep the tools as aligned with the specification as possible.
When you suspect there is a flaw in one of the tools, please report that as soon as possible.

### Connection tests
These tests deal with how SiLA Clients connect to SiLA Servers:

- Encrypted and unencrypted communication
- Direct connection and SiLA Server Discovery
- Client- and server-initiated connection

### Communication tests
These tests deal with the gRPC-based communication between SiLA Clients and SiLA Servers:

- Request messages
- Response messages
- Request metadata
- Response error statuses

Here again, both client- and server-initiated connection methods shall be tested

### Reporting
Every implementation shall execute the provided testing tools at their own discretion, e.g. during the release process.
These tools generate reports, of which the latest version has to be made available publicly, e.g. as CI artifact.
This repository shall collect and aggregate all reports to generate a unified report on the interoperability of the existing SiLA 2 implementations.

## Provided tools
NOTE: These tools are only first drafts and test a minimal subset of the specification.

### Communication testing
#### Installation
The tool requires Python >= 3.9.
Download the package from [here](https://gitlab.com/SiLA2/sila_interoperability/-/jobs/artifacts/main/raw/sila2-interop-communication-tester.whl?job=build-python),
then install it with `pip install sila2-interop-communication-tester.whl`.

#### How to test your server
1. Start your server
2. Execute the test client with `python -m sila2_interop_communication_tester.test_client [options]` (see below for options)
   - The tool acts as SiLA Client: It sends requests to the server and checks if the responses were as expected
   - It will print a report and, depending on the given options, also export one
3. Stop your server

Options:
- `--help`
- `--report-file PATH`: Generate a report file
- `--testsuite-name NAME`: Name of the testsuite in the generated report
- `--server-address ADDRESS`: Address where the server is running (default: `127.0.0.1:50052`)
- `--roots-cert-file PATH`: Path to a PEM-encoded root certificate file for connecting to the server (default: unencrypted communication)

#### How to test your client
1. Start the test server with `python -m sila2_interop_communication_tester.test_server [options]` (see below for options)
   - The tool acts as SiLA Server: While it is running, it records the requests it received
2. Execute your client program
3. Interrupt the test server process (Ctrl+C, `kill -INT [PID]`, ...)
   - The tool will stop the gRPC server and analyze the requests it received
   - It will print a report and, depending on the given options, also export one

Options:
- `--help`
- `--report-file PATH`: Generate a report file
- `--testsuite-name NAME`: Name of the testsuite in the generated report
- `--server-address ADDRESS`: Address where the server is running (default: `127.0.0.1:50052`)
- `--cert-file PATH`: Path to a PEM-encoded certificate file (default: unencrypted communication)
- `--key-file PATH`: Path to a PEM-encoded private key file (default: unencrypted communication)

#### Integration in CI pipelines
**Install Python**
In Debian-based Docker images, Python and Pip can be installed with `apt install python3-pip python3-is-python`.
Alpine images are not recommended, since some dependencies might be incompatible with musl libc.

**Install tool**
Run the following commands to download and install the tool:
```shell
wget https://gitlab.com/SiLA2/sila_interoperability/-/jobs/artifacts/main/raw/sila2-interop-communication-tester.whl?job=build-python
pip install sila2-interop-communication-tester.whl
```

**Run tool**
Executing server and client processes in parallel works as follows:
```shell
# start server process, redirect all output to a file `server.log`, detach the process, store its process ID in `server_pid`
start-server > server.log 2>&1 & server_pid=$!
# wait for server startup (adjust time as suitable)
sleep 10
# run client
run-client
# terminate server process (`-INT` is Ctrl+C, `-TERM` is default behavior)
kill -INT $server_pid
```
