# -*- coding: utf-8 -*-
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: UnobservableCommandTest.proto
"""Generated protocol buffer code."""
from google.protobuf import descriptor as _descriptor
from google.protobuf import descriptor_pool as _descriptor_pool
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()


from . import SiLAFramework_pb2 as SiLAFramework__pb2


DESCRIPTOR = _descriptor_pool.Default().AddSerializedFile(b'\n\x1dUnobservableCommandTest.proto\x12\x36sila2.org.silastandard.test.unobservablecommandtest.v1\x1a\x13SiLAFramework.proto\"1\n/CommandWithoutParametersAndResponses_Parameters\"0\n.CommandWithoutParametersAndResponses_Responses\"U\n!ConvertIntegerToString_Parameters\x12\x30\n\x07Integer\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\"`\n ConvertIntegerToString_Responses\x12<\n\x14StringRepresentation\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"\x83\x01\n\x1fJoinIntegerAndString_Parameters\x12\x30\n\x07Integer\x18\x01 \x01(\x0b\x32\x1f.sila2.org.silastandard.Integer\x12.\n\x06String\x18\x02 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"Z\n\x1eJoinIntegerAndString_Responses\x12\x38\n\x10JoinedParameters\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"[\n)SplitStringAfterFirstCharacter_Parameters\x12.\n\x06String\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\"\x95\x01\n(SplitStringAfterFirstCharacter_Responses\x12\x36\n\x0e\x46irstCharacter\x18\x01 \x01(\x0b\x32\x1e.sila2.org.silastandard.String\x12\x31\n\tRemainder\x18\x02 \x01(\x0b\x32\x1e.sila2.org.silastandard.String2\x9d\x07\n\x17UnobservableCommandTest\x12\xf9\x01\n$CommandWithoutParametersAndResponses\x12g.sila2.org.silastandard.test.unobservablecommandtest.v1.CommandWithoutParametersAndResponses_Parameters\x1a\x66.sila2.org.silastandard.test.unobservablecommandtest.v1.CommandWithoutParametersAndResponses_Responses\"\x00\x12\xcf\x01\n\x16\x43onvertIntegerToString\x12Y.sila2.org.silastandard.test.unobservablecommandtest.v1.ConvertIntegerToString_Parameters\x1aX.sila2.org.silastandard.test.unobservablecommandtest.v1.ConvertIntegerToString_Responses\"\x00\x12\xc9\x01\n\x14JoinIntegerAndString\x12W.sila2.org.silastandard.test.unobservablecommandtest.v1.JoinIntegerAndString_Parameters\x1aV.sila2.org.silastandard.test.unobservablecommandtest.v1.JoinIntegerAndString_Responses\"\x00\x12\xe7\x01\n\x1eSplitStringAfterFirstCharacter\x12\x61.sila2.org.silastandard.test.unobservablecommandtest.v1.SplitStringAfterFirstCharacter_Parameters\x1a`.sila2.org.silastandard.test.unobservablecommandtest.v1.SplitStringAfterFirstCharacter_Responses\"\x00\x62\x06proto3')



_COMMANDWITHOUTPARAMETERSANDRESPONSES_PARAMETERS = DESCRIPTOR.message_types_by_name['CommandWithoutParametersAndResponses_Parameters']
_COMMANDWITHOUTPARAMETERSANDRESPONSES_RESPONSES = DESCRIPTOR.message_types_by_name['CommandWithoutParametersAndResponses_Responses']
_CONVERTINTEGERTOSTRING_PARAMETERS = DESCRIPTOR.message_types_by_name['ConvertIntegerToString_Parameters']
_CONVERTINTEGERTOSTRING_RESPONSES = DESCRIPTOR.message_types_by_name['ConvertIntegerToString_Responses']
_JOININTEGERANDSTRING_PARAMETERS = DESCRIPTOR.message_types_by_name['JoinIntegerAndString_Parameters']
_JOININTEGERANDSTRING_RESPONSES = DESCRIPTOR.message_types_by_name['JoinIntegerAndString_Responses']
_SPLITSTRINGAFTERFIRSTCHARACTER_PARAMETERS = DESCRIPTOR.message_types_by_name['SplitStringAfterFirstCharacter_Parameters']
_SPLITSTRINGAFTERFIRSTCHARACTER_RESPONSES = DESCRIPTOR.message_types_by_name['SplitStringAfterFirstCharacter_Responses']
CommandWithoutParametersAndResponses_Parameters = _reflection.GeneratedProtocolMessageType('CommandWithoutParametersAndResponses_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _COMMANDWITHOUTPARAMETERSANDRESPONSES_PARAMETERS,
  '__module__' : 'UnobservableCommandTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.unobservablecommandtest.v1.CommandWithoutParametersAndResponses_Parameters)
  })
_sym_db.RegisterMessage(CommandWithoutParametersAndResponses_Parameters)

CommandWithoutParametersAndResponses_Responses = _reflection.GeneratedProtocolMessageType('CommandWithoutParametersAndResponses_Responses', (_message.Message,), {
  'DESCRIPTOR' : _COMMANDWITHOUTPARAMETERSANDRESPONSES_RESPONSES,
  '__module__' : 'UnobservableCommandTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.unobservablecommandtest.v1.CommandWithoutParametersAndResponses_Responses)
  })
_sym_db.RegisterMessage(CommandWithoutParametersAndResponses_Responses)

ConvertIntegerToString_Parameters = _reflection.GeneratedProtocolMessageType('ConvertIntegerToString_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _CONVERTINTEGERTOSTRING_PARAMETERS,
  '__module__' : 'UnobservableCommandTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.unobservablecommandtest.v1.ConvertIntegerToString_Parameters)
  })
_sym_db.RegisterMessage(ConvertIntegerToString_Parameters)

ConvertIntegerToString_Responses = _reflection.GeneratedProtocolMessageType('ConvertIntegerToString_Responses', (_message.Message,), {
  'DESCRIPTOR' : _CONVERTINTEGERTOSTRING_RESPONSES,
  '__module__' : 'UnobservableCommandTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.unobservablecommandtest.v1.ConvertIntegerToString_Responses)
  })
_sym_db.RegisterMessage(ConvertIntegerToString_Responses)

JoinIntegerAndString_Parameters = _reflection.GeneratedProtocolMessageType('JoinIntegerAndString_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _JOININTEGERANDSTRING_PARAMETERS,
  '__module__' : 'UnobservableCommandTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.unobservablecommandtest.v1.JoinIntegerAndString_Parameters)
  })
_sym_db.RegisterMessage(JoinIntegerAndString_Parameters)

JoinIntegerAndString_Responses = _reflection.GeneratedProtocolMessageType('JoinIntegerAndString_Responses', (_message.Message,), {
  'DESCRIPTOR' : _JOININTEGERANDSTRING_RESPONSES,
  '__module__' : 'UnobservableCommandTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.unobservablecommandtest.v1.JoinIntegerAndString_Responses)
  })
_sym_db.RegisterMessage(JoinIntegerAndString_Responses)

SplitStringAfterFirstCharacter_Parameters = _reflection.GeneratedProtocolMessageType('SplitStringAfterFirstCharacter_Parameters', (_message.Message,), {
  'DESCRIPTOR' : _SPLITSTRINGAFTERFIRSTCHARACTER_PARAMETERS,
  '__module__' : 'UnobservableCommandTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.unobservablecommandtest.v1.SplitStringAfterFirstCharacter_Parameters)
  })
_sym_db.RegisterMessage(SplitStringAfterFirstCharacter_Parameters)

SplitStringAfterFirstCharacter_Responses = _reflection.GeneratedProtocolMessageType('SplitStringAfterFirstCharacter_Responses', (_message.Message,), {
  'DESCRIPTOR' : _SPLITSTRINGAFTERFIRSTCHARACTER_RESPONSES,
  '__module__' : 'UnobservableCommandTest_pb2'
  # @@protoc_insertion_point(class_scope:sila2.org.silastandard.test.unobservablecommandtest.v1.SplitStringAfterFirstCharacter_Responses)
  })
_sym_db.RegisterMessage(SplitStringAfterFirstCharacter_Responses)

_UNOBSERVABLECOMMANDTEST = DESCRIPTOR.services_by_name['UnobservableCommandTest']
if _descriptor._USE_C_DESCRIPTORS == False:

  DESCRIPTOR._options = None
  _COMMANDWITHOUTPARAMETERSANDRESPONSES_PARAMETERS._serialized_start=110
  _COMMANDWITHOUTPARAMETERSANDRESPONSES_PARAMETERS._serialized_end=159
  _COMMANDWITHOUTPARAMETERSANDRESPONSES_RESPONSES._serialized_start=161
  _COMMANDWITHOUTPARAMETERSANDRESPONSES_RESPONSES._serialized_end=209
  _CONVERTINTEGERTOSTRING_PARAMETERS._serialized_start=211
  _CONVERTINTEGERTOSTRING_PARAMETERS._serialized_end=296
  _CONVERTINTEGERTOSTRING_RESPONSES._serialized_start=298
  _CONVERTINTEGERTOSTRING_RESPONSES._serialized_end=394
  _JOININTEGERANDSTRING_PARAMETERS._serialized_start=397
  _JOININTEGERANDSTRING_PARAMETERS._serialized_end=528
  _JOININTEGERANDSTRING_RESPONSES._serialized_start=530
  _JOININTEGERANDSTRING_RESPONSES._serialized_end=620
  _SPLITSTRINGAFTERFIRSTCHARACTER_PARAMETERS._serialized_start=622
  _SPLITSTRINGAFTERFIRSTCHARACTER_PARAMETERS._serialized_end=713
  _SPLITSTRINGAFTERFIRSTCHARACTER_RESPONSES._serialized_start=716
  _SPLITSTRINGAFTERFIRSTCHARACTER_RESPONSES._serialized_end=865
  _UNOBSERVABLECOMMANDTEST._serialized_start=868
  _UNOBSERVABLECOMMANDTEST._serialized_end=1793
# @@protoc_insertion_point(module_scope)
