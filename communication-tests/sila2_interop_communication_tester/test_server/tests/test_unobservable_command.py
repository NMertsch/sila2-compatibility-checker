def test_command_called(method_call_entries):
    calls = method_call_entries["UnobservableCommandTest.CommandWithoutParametersAndResponses"]
    assert len(calls) == 1, "UnobservableCommandTest.CommandWithoutParametersAndResponses was not called exactly once"
