def test_property_accessed(method_call_entries):
    calls = method_call_entries["UnobservablePropertyTest.Get_AnswerToEverything"]
    assert len(calls) == 1, "UnobservablePropertyTest.Get_AnswerToEverything was not called exactly once"
