import pytest

from sila2_interop_communication_tester.test_server.helpers.spy import MethodCallEntry

# is set in __main__.py
RPC_CALL_ARGS: dict[str, list[MethodCallEntry]]


@pytest.fixture(scope="session")
def method_call_entries() -> dict[str, list[MethodCallEntry]]:
    return RPC_CALL_ARGS  # noqa: F821
