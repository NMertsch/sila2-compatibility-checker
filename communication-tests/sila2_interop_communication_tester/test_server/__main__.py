from __future__ import annotations

import concurrent.futures
import os
import signal
from argparse import ArgumentParser
from glob import glob
from os.path import abspath, dirname, join

import grpc
from _pytest.config import ExitCode
from _pytest.config import main as pytest_main

from sila2_interop_communication_tester.grpc_stubs.SiLAService_pb2_grpc import add_SiLAServiceServicer_to_server
from sila2_interop_communication_tester.grpc_stubs.UnobservableCommandTest_pb2_grpc import (
    UnobservableCommandTestServicer,
    add_UnobservableCommandTestServicer_to_server,
)
from sila2_interop_communication_tester.grpc_stubs.UnobservablePropertyTest_pb2_grpc import (
    UnobservablePropertyTestServicer,
    add_UnobservablePropertyTestServicer_to_server,
)
from sila2_interop_communication_tester.test_server.helpers.spy import ARGS_DICT, spy_servicer
from sila2_interop_communication_tester.test_server.server.sila_service import SiLAServiceImpl
from sila2_interop_communication_tester.test_server.tests import conftest


def main(args: list[str] | None = None):
    # parse args
    parser = ArgumentParser(
        description="SiLA 2 client for testing server implementations",
    )
    parser.add_argument("--report-file", default=None, help="If set, generate JUnit-like XML report file")
    parser.add_argument("--testsuite-name", default=None, help="Testsuite name for report file")
    parser.add_argument(
        "--server-address", default="127.0.0.1:50052", help="Server address (default: '127.0.0.1:50052')"
    )
    parser.add_argument(
        "--cert-file",
        default=None,
        help="PEM-encoded certificate file. If none, unencrypted communication is used",
    )
    parser.add_argument(
        "--key-file",
        default=None,
        help="PEM-encoded private key file. If none, unencrypted communication is used",
    )
    parsed_args = parser.parse_args(args)

    # create server
    server = grpc.server(concurrent.futures.ThreadPoolExecutor(max_workers=100))

    # configure server
    if parsed_args.cert_file is not None and parsed_args.key_file is not None:
        with open(parsed_args.cert_file, "rb") as cert_fp, open(parsed_args.key_file, "rb") as key_fp:
            server.add_secure_port(
                parsed_args.server_address,
                server_credentials=grpc.ssl_server_credentials(key_fp.read(), cert_fp.read()),
            )
    if parsed_args.cert_file is not None or parsed_args.key_file is not None:
        raise ValueError("Either certificate and private key files must both be provided, or none of them")
    else:
        server.add_insecure_port(parsed_args.server_address)

    # add features with custom implementation
    add_SiLAServiceServicer_to_server(spy_servicer(SiLAServiceImpl()), server)

    # add features without implementation (requests will be recorded, client will receive errors)
    add_UnobservablePropertyTestServicer_to_server(spy_servicer(UnobservablePropertyTestServicer()), server)
    add_UnobservableCommandTestServicer_to_server(spy_servicer(UnobservableCommandTestServicer()), server)

    # start server
    server.start()
    print("Server started")

    # wait for shutdown
    signal.signal(signal.SIGINT, lambda *_: server.stop(10))  # handle interrupt signal (Ctrl+C)
    signal.signal(signal.SIGTERM, lambda *_: server.stop(10))  # handle termination signal
    server.wait_for_termination()
    print("Server stopped")

    # switch to module directory
    original_dir = abspath(".")
    os.chdir(dirname(__file__))
    test_directories: list[str] = glob("tests/test_*/")
    pytest_options: list[str] = [
        "-r A",  # print verbose test summary
    ]

    if parsed_args.report_file is not None:
        pytest_options.append(f"--junitxml={join(original_dir, parsed_args.report_file)}")
    if parsed_args.testsuite_name is not None:
        pytest_options.append("-o")
        pytest_options.append(f"junit_suite_name={parsed_args.testsuite_name}")

    conftest.RPC_CALL_ARGS = ARGS_DICT

    return pytest_main(
        args=test_directories[:1] + pytest_options,
    )


if __name__ == "__main__":
    code = main()
    if isinstance(code, ExitCode):
        code = code.value

    exit(code)
