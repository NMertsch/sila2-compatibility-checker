from collections import defaultdict
from datetime import datetime
from functools import wraps
from typing import NamedTuple

from google.protobuf.message import Message
from grpc import ServicerContext


class MethodCallEntry(NamedTuple):
    timestamp: datetime
    request: Message
    context: ServicerContext
    metadata: dict[str, bytes]


ARGS_DICT: dict[str, list[MethodCallEntry]] = defaultdict(list)


def spy_servicer(servicer):
    for name in dir(servicer):
        binding = getattr(servicer, name)
        if callable(binding) and name[0].isupper():
            setattr(servicer, name, _spy_method(binding))
    return servicer


def _spy_method(rpc_method):
    @wraps(rpc_method)
    def wrapper(*args):
        request, context = args[-2:]  # allows for signatures (self, request, context) and (request, context)

        metadata = {}
        for key, value in context.invocation_metadata():
            key: str
            if key.startswith("sila-") and key.endswith("-bin"):
                value: bytes
                metadata[key] = value

        method_name = rpc_method.__qualname__.replace("Impl.", ".").replace("Servicer.", ".")

        ARGS_DICT[method_name].append(MethodCallEntry(datetime.now(), request, context, metadata))
        return rpc_method(*args)

    return wrapper
