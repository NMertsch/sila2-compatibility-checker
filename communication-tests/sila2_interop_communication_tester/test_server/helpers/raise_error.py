from base64 import standard_b64encode
from typing import NoReturn

from grpc import ServicerContext, StatusCode

from sila2_interop_communication_tester.grpc_stubs.SiLAFramework_pb2 import (
    DefinedExecutionError,
    FrameworkError,
    SiLAError,
    UndefinedExecutionError,
    ValidationError,
)


def _raise_as_rpc_error(context: ServicerContext, error_message: SiLAError) -> NoReturn:
    context.abort(StatusCode.ABORTED, details=standard_b64encode(error_message.SerializeToString()).decode("ascii"))


def raise_validation_error(context: ServicerContext, parameter_id: str, message: str) -> NoReturn:
    _raise_as_rpc_error(context, SiLAError(validationError=ValidationError(parameter=parameter_id, message=message)))


def raise_framework_error(context: ServicerContext, error_type: FrameworkError.ErrorType, message: str) -> NoReturn:
    _raise_as_rpc_error(context, SiLAError(frameworkError=FrameworkError(errorType=error_type, message=message)))


def raise_undefined_execution_error(context: ServicerContext, message: str) -> NoReturn:
    _raise_as_rpc_error(context, SiLAError(undefinedExecutionError=UndefinedExecutionError(message=message)))


def raise_defined_execution_error(context: ServicerContext, error_id: str, message: str) -> NoReturn:
    _raise_as_rpc_error(
        context, SiLAError(definedExecutionError=DefinedExecutionError(errorIdentifier=error_id, message=message))
    )
