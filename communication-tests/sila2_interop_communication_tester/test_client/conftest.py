"""Pytest configuration file"""
import grpc
import pytest

# is set in __main__.py
CHANNEL: grpc.Channel


@pytest.fixture(scope="session")
def channel() -> grpc.Channel:
    return CHANNEL  # noqa: F821
