import contextlib
import re

from sila2_interop_communication_tester.grpc_stubs import SiLAFramework_pb2, SiLAService_pb2
from sila2_interop_communication_tester.grpc_stubs.SiLAService_pb2_grpc import SiLAServiceStub
from sila2_interop_communication_tester.test_client.helpers import raises_validation_error


@contextlib.contextmanager
def reset_server_name(stub: SiLAServiceStub):
    old_server_name = stub.Get_ServerName(SiLAService_pb2.Get_ServerName_Parameters()).ServerName.value

    try:
        yield
    finally:
        stub.SetServerName(
            SiLAService_pb2.SetServerName_Parameters(ServerName=SiLAFramework_pb2.String(value=old_server_name))
        )


def test_get_server_name(silaservice_stub):
    server_name = silaservice_stub.Get_ServerName(SiLAService_pb2.Get_ServerName_Parameters()).ServerName.value
    assert len(server_name) > 0
    assert len(server_name) <= 255


def test_get_server_type(silaservice_stub):
    server_type = silaservice_stub.Get_ServerType(SiLAService_pb2.Get_ServerType_Parameters()).ServerType.value
    match = re.fullmatch(r"[A-Z][a-zA-Z\d]*", server_type)
    assert match is not None


def test_get_server_version(silaservice_stub):
    server_version = silaservice_stub.Get_ServerVersion(
        SiLAService_pb2.Get_ServerVersion_Parameters()
    ).ServerVersion.value
    match = re.fullmatch(r"(0|[1-9]\d*)\.(0|[1-9]\d*)(\.(0|[1-9]\d*))?(_\w+)?", server_version)
    assert match is not None


def test_get_server_vendor_url(silaservice_stub):
    server_vendor_url = silaservice_stub.Get_ServerVendorURL(
        SiLAService_pb2.Get_ServerVendorURL_Parameters()
    ).ServerVendorURL.value
    match = re.fullmatch(r"https?://.+", server_vendor_url)
    assert match is not None


def test_get_server_description(silaservice_stub):
    server_description = silaservice_stub.Get_ServerDescription(
        SiLAService_pb2.Get_ServerDescription_Parameters()
    ).ServerDescription.value
    assert len(server_description) > 0


def test_get_server_uuid(silaservice_stub):
    server_uuid = silaservice_stub.Get_ServerUUID(SiLAService_pb2.Get_ServerUUID_Parameters()).ServerUUID.value
    assert len(server_uuid) == 36
    assert re.fullmatch(r"[\da-f]{8}-[\da-f]{4}-[\da-f]{4}-[\da-f]{4}-[\da-f]{12}", server_uuid)


def test_set_server_name_sets_name(silaservice_stub):
    with reset_server_name(silaservice_stub):
        old_name: str = silaservice_stub.Get_ServerName(SiLAService_pb2.Get_ServerName_Parameters()).ServerName.value
        new_name = old_name[:-1]

        silaservice_stub.SetServerName(
            SiLAService_pb2.SetServerName_Parameters(ServerName=SiLAFramework_pb2.String(value=new_name))
        )

        assert silaservice_stub.Get_ServerName(SiLAService_pb2.Get_ServerName_Parameters()).ServerName.value == new_name


def test_set_server_name_with_too_long_parameter_raises_validation_error(silaservice_stub):
    with reset_server_name(silaservice_stub):
        with raises_validation_error() as sila_error:
            silaservice_stub.SetServerName(
                SiLAService_pb2.SetServerName_Parameters(ServerName=SiLAFramework_pb2.String(value="H" * 256))
            )
        assert sila_error.error.parameter == (
            "org.silastandard/core/SiLAService/v1/Command/SetServerName/Parameter/ServerName"
        )


def test_set_server_name_with_empty_message_raises_validation_error(silaservice_stub):
    with reset_server_name(silaservice_stub):
        old_server_name = silaservice_stub.Get_ServerName(SiLAService_pb2.Get_ServerName_Parameters()).ServerName.value

        with raises_validation_error() as sila_error:
            silaservice_stub.SetServerName(SiLAService_pb2.SetServerName_Parameters())

        assert sila_error.error.parameter == (
            "org.silastandard/core/SiLAService/v1/Command/SetServerName/Parameter/ServerName"
        )

        new_server_name = silaservice_stub.Get_ServerName(SiLAService_pb2.Get_ServerName_Parameters()).ServerName.value
        assert old_server_name == new_server_name
