from __future__ import annotations

import os
from argparse import ArgumentParser
from glob import glob
from os.path import abspath, dirname, join

import grpc
from _pytest.config import ExitCode
from _pytest.config import main as pytest_main

from . import conftest


def main(args: list[str] | None = None) -> int | ExitCode:
    parser = ArgumentParser(
        description="SiLA 2 client for testing server implementations",
    )
    parser.add_argument("--report-file", default=None, help="If set, generate JUnit-like XML report file")
    parser.add_argument("--testsuite-name", default=None, help="Testsuite name for report file")
    parser.add_argument(
        "--server-address", default="127.0.0.1:50052", help="Server address (default: '127.0.0.1:50052')"
    )
    parser.add_argument(
        "--roots-cert-file",
        default=None,
        help="PEM-encoded root certificates file. If none, unencrypted communication is attempted",
    )
    parsed_args = parser.parse_args(args)

    original_dir = abspath(".")

    # switch to module directory
    os.chdir(dirname(__file__))
    test_directories: list[str] = glob("test_*/")
    pytest_options: list[str] = [
        "-r A",  # print verbose test summary
    ]

    if parsed_args.report_file is not None:
        pytest_options.append(f"--junitxml={join(original_dir, parsed_args.report_file)}")
    if parsed_args.testsuite_name is not None:
        pytest_options.append("-o")
        pytest_options.append(f"junit_suite_name={parsed_args.testsuite_name}")

    if parsed_args.roots_cert_file is None:
        conftest.CHANNEL = grpc.insecure_channel(parsed_args.server_address)
    else:
        with open(parsed_args.roots_cert_file, "rb") as roots_cert_pem_fp:
            conftest.CHANNEL = grpc.secure_channel(
                parsed_args.server_address, credentials=grpc.ssl_channel_credentials(roots_cert_pem_fp.read())
            )

    return pytest_main(
        args=test_directories[:1] + pytest_options,
    )


if __name__ == "__main__":
    code = main()
    if isinstance(code, ExitCode):
        code = code.value

    exit(code)
