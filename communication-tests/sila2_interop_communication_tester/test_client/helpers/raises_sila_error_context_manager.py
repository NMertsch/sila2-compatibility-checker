"""Defines context managers to assert that SiLA Errors are raised"""
import binascii
from base64 import standard_b64decode
from types import TracebackType
from typing import Generic, Literal, Optional, Type, TypeVar

import google.protobuf.message
import grpc
from pytest import fail

from sila2_interop_communication_tester.grpc_stubs.SiLAFramework_pb2 import (
    DefinedExecutionError,
    FrameworkError,
    SiLAError,
    UndefinedExecutionError,
    ValidationError,
)

_SiLAErrorType = TypeVar(
    "_SiLAErrorType", ValidationError, FrameworkError, DefinedExecutionError, UndefinedExecutionError
)
_SiLAErrorTypeName = Literal["validationError", "frameworkError", "definedExecutionError", "undefinedExecutionError"]


class SiLAErrorOption(Generic[_SiLAErrorType]):
    def __init__(self):
        self.error: Optional[_SiLAErrorType] = None


class RaisesContext(Generic[_SiLAErrorType]):
    # adapted from pytest.raises
    def __init__(self, error_type: _SiLAErrorTypeName) -> None:
        self.error_type = error_type
        self.sila_error_option: SiLAErrorOption[_SiLAErrorType] = SiLAErrorOption()

    def __enter__(self) -> SiLAErrorOption[_SiLAErrorType]:
        return self.sila_error_option

    def __exit__(
        self,
        exc_type: Optional[Type[BaseException]],
        exc_val: Optional[BaseException],
        exc_tb: Optional[TracebackType],
    ) -> bool:
        __tracebackhide__ = True

        if exc_type is None:
            fail("No exception was raised")

        if not issubclass(exc_type, grpc.RpcError):
            return False

        assert isinstance(exc_val, grpc.RpcError)
        assert isinstance(exc_val, grpc.Call)
        assert exc_val.code() == grpc.StatusCode.ABORTED, "SiLA Errors must have the gRPC status code 'ABORTED'"

        try:
            sila_error = SiLAError.FromString(standard_b64decode(exc_val.details()))
        except binascii.Error:
            fail("Failed to decode error details as Base64")
        except google.protobuf.message.DecodeError:
            fail("Failed to decode error details as SiLAFramework.SiLAError Protobuf message")

        assert sila_error.HasField(self.error_type), f"Caught SiLA Error is no {self.error_type}"

        specific_error: _SiLAErrorType = getattr(sila_error, self.error_type)
        assert len(specific_error.message) > 10, "SiLA Errors must include information about the error"

        self.sila_error_option.error = specific_error
        return True


def raises_defined_execution_error() -> RaisesContext[DefinedExecutionError]:
    return RaisesContext("definedExecutionError")


def raises_undefined_execution_error() -> RaisesContext[UndefinedExecutionError]:
    return RaisesContext("undefinedExecutionError")


def raises_validation_error() -> RaisesContext[ValidationError]:
    return RaisesContext("validationError")


def raises_framework_error() -> RaisesContext[FrameworkError]:
    return RaisesContext("frameworkError")
