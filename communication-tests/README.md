# Communication tests
This directory contains a tool for testing SiLA 2 communication.

## Development setup
- Install Python >= 3.9
- Open a terminal, navigate to this directory (containing `setup.py`)
- Run `pip install -e .[dev]` for an editable installation with all development dependencies

## Add/update feature code
- Run `python add-features.py FDL-FILES`, it will convert .sila.xml files to .proto and .py files

## Code quality
- To auto-format the Python code, run `black .` and `isort .`
- To analyze for common code smells, run `pflake8`
